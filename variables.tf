variable "registry_name" {
  type        = string
  description = "Registry name of the container registry"
}

variable "registry_sku" {
  type        = string
  description = "SKU name of the container registry"
  default     = "Standard"
}
